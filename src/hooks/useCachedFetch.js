import {useState, useEffect} from 'react';

export function useCachedFetch(url) {
  const [data, storeData] = useState(null);

  useEffect(() => {
    console.log('useCachedFetch effect called');
    if (!data) {
      fetch(url)
        .then(response => response.json())
        .then(data => {
          storeData(data);
        });
    }

    return () => {
      console.log('cancel pending promises');
    }
  });

  return data;
}
