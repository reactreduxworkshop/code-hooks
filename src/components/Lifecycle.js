import React, {useEffect} from 'react';

let callNo = 1;

function Lifecycle() {
  console.log('Lifecycle render');

  useEffect(() => {
    if (callNo === 1) {
      console.log('first call');
    } else {
      console.log('update')
    }

    callNo++;

    return () => {
      console.log('cleanup');
    }
  }, [callNo > 2 ? null : callNo]);

  return (
    <span>Lifecycle</span>
  );
}

export default Lifecycle;