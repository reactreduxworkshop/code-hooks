import React, {useState} from 'react';

function CounterObject() {
  const [counter, setCounter] = useState({
    value: 0,
    otherValue: 11,
  });

  // hooki obowiązuje ta sama zasada co setState - gdy bazują na aktualnej wartości musi być użyty zapis z funkcją
  const handleClick = () => {
    setCounter(counter => ({...counter, value: counter.value + 1}));
    setCounter(counter => ({...counter, value: counter.value + 1}));
  };

  return (
    <React.Fragment>
      <div>Counter: {counter.value}</div>
      <button onClick={handleClick}>Increment</button>
    </React.Fragment>
  )
}

export default CounterObject;