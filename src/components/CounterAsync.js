import React, {useState, useEffect} from 'react';

function CounterAsync() {
  const [counter, setCounter] = useState(null);

  useEffect(() => {
    new Promise((res, rej) => {
      setTimeout(() => setCounter(0), 4000);
    });
  }, []);

  // hooki obowiązuje ta sama zasada co setState - gdy bazują na aktualnej wartości musi być użyty zapis z funkcją
  const handleClick = () => {
    setCounter(counter => counter + 1);
    setCounter(counter => counter + 1);
  };

  return (
    <React.Fragment>
      <div>Counter: {counter}</div>
      <button onClick={handleClick} disabled={counter === null}>Increment</button>
    </React.Fragment>
  )
}

export default CounterAsync;