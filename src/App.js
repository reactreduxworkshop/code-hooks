import React, {useState} from 'react';

import Counter from "./components/Counter";
import CounterAsync from "./components/CounterAsync";
import CounterObject from "./components/CounterObject";
import Lifecycle from "./components/Lifecycle";
import FetchData from "./components/FetchData";

function App() {
  const [val, setVal] = useState(0);

  return (
    <div>
      <Counter/>
      <CounterAsync/>
      <CounterObject/>
      <br/>
      Lifecycles: {val} {val < 8 ? <Lifecycle/> : null}
      <button onClick={() => setVal(val => val + 1)}>Rerender</button>
      <br/>
      <FetchData/>
    </div>
  )
}

export default App;
